\section{Methodology}

This section describes the three models we proposed to learn time-aware
representations for knowledge graphs.
%The training method and how to construct
%training sets are also explained.

\subsection{Problem Definition and Notations}
Denote a \textit{temporal knowledge graph} as $\mathcal{G}=\{(h,r,t, \tau_s,
\tau_e)\}\subseteq \mathcal{E} \times \mathcal{R} \times \mathcal{E} \times \mathcal{T} \times \mathcal{T}$,
%where $\mathcal{E}$ denotes the set of entities, $\mathcal{R}$ denotes the set of relations and $\mathcal{T}$ denotes the set of timestamps,
where $\mathcal{E}$, $\mathcal{R}$ and $\mathcal{T}$ denotes the set of entities, relations, and timestamps respectively.
$h,t \in \mathcal{E}$, $r\in \mathcal{R}$, $\tau_s, \tau_e \in \mathcal{T}$. 
A tuple $(h,r,t,\tau_s, \tau_e)$ represents a time-aware fact that head entity $h$ and tail entity
$t$ have relation $r$ during time scope $\tau_s$ to $\tau_e$.
Given a time-aware fact $(h,r,t,\tau_s, \tau_e)$,
we can equivalently describe it as a set of quadruples $\{(h,r,t,\tau_{k_1}),
 \ \cdots, (h,r,t,\tau_{k_n})\}$,
if we let $(h,r,t,\tau_{k_i})$ denote head entity $h$ and tail entity
$t$ have relation $r$ at the timestamp $\tau_{k_i}$, 
where $\tau_{k_1}, \tau_{k_2}, \ \cdots, \tau_{k_n}$ are timestamps included in time scope $\tau_s$ to
$\tau_e$. In the following, we will adopt quadruple $(h,r,t,\tau)$ to
formulate our models.

In many representation learning methods or approaches, all entities and
relations are considered lying in the
same space. However, in this paper, we differentiate the entity space and the
relation space to extend the expressive power of the proposed models 
by allowing them to be affected by different transformation rules.

For each fact $(h,r,t)$ in a given knowledge graph, 
the \textit{time-aware representation learning} aims to learn time-specific
embeddings $\bm{h}_{\tau_k}$ and $\bm{t}_{\tau_k}$for entities $h$ and
$t$, and $\bm{r}_{\tau_k}$ for relation $r$ when given a timestamp $\tau_k\in\mathcal{T}$, where $k=1, 2, \ \cdots, N$,
$N\in\mathbb{N}$.
In general, all entities and relations can evolve over time. 
Therefore, it is not affordable to track all the entity and relation vectors at each
timestamp. To address this challenge, we assume that for each entity and relation, there is a constant embedding vector which is employed to represent it.
We denote $\bm{h}, \bm{t} \in \mathbb{R}^{k_e}$ as the constant embeddings of entities $h$ and $t$,
$\bm{r} \in \mathbb{R}^{k_r}$ as the constant embedding of relation $r$,
where $k_e$ is the dimension of the entity space $\mathbb{R}^{k_e}$, and $k_r$ is the dimension of
the relation space $\mathbb{R}^{k_r}$.
For each timestamp $\tau_k$, to obtain the time-specific embeddings, 
we will learn a time-specific transformation to convert the constant embeddings,
$\bm{h}, \bm{t}, \bm{r}$, into the time-specific embeddings, $\bm{h}_{\tau_k},
\bm{t}_{\tau_k}, \bm{r}_{\tau_k}$. 
In this paper, we adorp the convention that all embeddings are represented by row vectors.

\subsection{Models}

According to the above description, we propose a model that considering both entities and relations are time-sensitive.
Therefore we call this the \both\ (i.e.,
Both Time-Sensitive) model. There are two simplifications toward the
\both{} model, one assumes only entities are time-sensitive and the other
considers only relations are time-sensitive. We call the former the \ent\ (i.e., Entity
Time-Sensitive) model and the latter the \rel\ (i.e., Relation Time-Sensitive) model.

\subsubsection{Model I: \both}
\both\ assumes that both entities and relations are affected by time.
At a specific timestamp $\tau_k$,
we project the entity and relation embeddings into a new space respectively.
We refer to it as ``time space'' in this paper.
This model is illustrated in Fig \ref{both}.

\input{figBoth}

At time $\tau_k$,
we denote $M_{e\tau_k} \in \mathbb{R}^{k_e \times k_t}$ as the projection matrix
from the entity space to the time space,
and $M_{r\tau_k} \in \mathbb{R}^{k_r \times k_t}$ as the projection matrix from
the relation space to the time space,
where $k_t \in \mathbb{R}$ is the dimension of the time space.
Then time specific embeddings of entities and relations at timestamp $\tau_k$
can be obtained by the following projection transformation.
\begin{align}
    \bm{h}_{\tau_k} &= \bm{h}M_{e \tau_k} , \\
    \bm{r}_{\tau_k} &= \bm{r}M_{r \tau_k} , \\
    \bm{t}_{\tau_k} &= \bm{t}M_{e \tau_k} .
\end{align}

\subsubsection{Model II: \ent}
For model \ent,
we assume that entities and relations belong to different spaces
and only the entity space is affected by time.
At a specific timestamp,
we project entity embeddings into the relation space based on a time-specific transformation matrix.
Illustration of the model is shown in Fig \ref{ent}.

\input{figEnt}

We denote $M_{\tau_k} \in \mathbb{R}^{k_e \times k_r}$ as the projection matrix at time $\tau_k$,
which projects embeddings from the entity space to the relation space.
The time-specific entity embeddings at time $\tau_k$ can be computed by the
follow project transformation:
\begin{align}
    \bm{h}_{\tau_k} &= \bm{h}M_{\tau_k} , \\
    \bm{t}_{\tau_k} &= \bm{t}M_{\tau_k} .
\end{align}
Since relation embedding is not affected by time, it equals to its corresponding
constant embedding,
\begin{align}
    \bm{r}_{\tau_k} = \bm{r}.
\end{align}

\subsubsection{Model III: \rel}
In the \rel\ model, we still put entities and relations in two different spaces,
while only relation space is affected by time.
At a specific time $\tau_k$,
we project relation embeddings into the entity space based on time-specific transformation matrix.
The basic idea is illustrated in Fig \ref{rel}.

\input{figRel}

We denote $M_{\tau_k} \in \mathbb{R}^{k_r \times k_e}$ as the projection matrix at time $\tau_k$,
which projects embeddings from the relation space to the entity space.
Then the relation embedding at time $\tau_k$ is
\begin{align}
    \bm{r}_{\tau_k} = \bm{r}M_{\tau_k} .
\end{align}
Embeddings of entities $h$ and $t$ always stay the same as they are not affected by
time,
\begin{align}
    \bm{h}_{\tau_k} &= \bm{h} , \\
    \bm{t}_{\tau_k} &= \bm{t} .
\end{align}

\subsection{Model Training}
For all three models constructed above,
we want the projected embeddings at a specific time $\tau_k$ to satisfy the TransE hypothesis,
i.e.,
relation embeddings can be seen as translation vectors from head entities to
tail entities
\begin{align}
    \bm{h}_{\tau_k} + \bm{r}_{\tau_k} \approx \bm{t}_{\tau_k},
\end{align}
which has been proved simple and effective in various translational representation learning models.

To achieve this goal,
we define a score function 
$\varphi: \mathcal{E} \times \mathcal{R} \times \mathcal{E} \times \mathcal{T} \to \mathbb{R}$
for quadruple $(h,r,t, \tau)$.
The score is calculated based on the learned time-specific embeddings
$\bm{h}_{\tau_k}$, $\bm{r}_{\tau_k}$, and $\bm{t}_{\tau_k}$.
For a given quadruple,
its score is proportional to the likelihood of it being correct.
We want the score to be lower for correct quadruples in the given knowledge graph
and higher for false quadruples not in the given knowledge graph.
In this paper, we apply the same score function for all three models
\begin{align}
    f_{\tau_k}(h,r,t) = \| \bm{h}_{\tau_k} + \bm{r}_{\tau_k}- \bm{t}_{\tau_k} \| .
\end{align}
Note that the embeddings $\bm{h}_{\tau_k}, \bm{r}_{\tau_k}$, and $\bm{t}_{\tau_k}$ are normalized before calculating scores.

We train our models by minimizing a margin-based ranking loss over the training set
\begin{align}
L = &\sum_{(h,r,t)\in \mathcal{S}} \sum_{(h',r',t')\in \mathcal{S'}} \max(0, [\gamma + \\ \notag 
& f_{\tau_k}(h,r,t) - f_{\tau_k}(h',r',t')]) .
\end{align}
where $\gamma$ is the margin, $\max(0,x)$ returns the positive part of $x$,
$\mathcal{S}$ is the set of correct quadruples in the given knowledge graph,
and $\mathcal{S^\prime}$ is the set of false quadruples constructed by negative sampling.

\input{tableEntity}
\input{tableStatistics}

There are two ways to construct $\mathcal{S}^\prime$ in the literature.
%One widely used way in existing knowledge graph representation learning
The widely adopted way is
\begin{align}
	\mathcal{S}_{h,r,t,\tau}^{'} = &\{(h', r, t, \tau)|h' \in \mathcal{E},
	(h',r,t) \notin \mathcal{S}\} \\ \notag 
        \cup &\{(h,r,t',\tau)|t' \in \mathcal{E},
	(h,r,t') \notin \mathcal{S}\}.
\end{align}
It ignores temporal information and selects triples that do not exist in the whole knowledge graph.
We use this negative sampling technique in both entity and relation
prediction tasks to evaluate the learned embeddings.

In time scope prediction task,
we use time dependent negative sampling \cite{HyTE}.
For a given quadruple $(h,r,t,\tau)$,
besides the negative samples obtained by the above method,
it also samples negative quadruples that exist in knowledge graphs at times other than $\tau$.
These extra negative samples are obtained as following:

\begin{align}
    \mathcal{S}_{h,r,t,\tau}^{'} = &\{(h', r, t, \tau)|h' \in \mathcal{E},\\ \notag
    & (h',r,t) \in \mathcal{S},
    (h',r,t,\tau) \notin \mathcal{S}_{\tau} \} \cup \\ \notag
    & \{(h,r,t',\tau)|t' \in \mathcal{E}, \\ \notag
    & (h,r,t') \in \mathcal{S}, (h,r,t',\tau) \notin \mathcal{S}_{\tau} \} .
\end{align}

We train our models using gradient descent over training set
and choose optimal parameters on validation set.
