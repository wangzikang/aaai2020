\section{Introduction}

A knowledge graph is a large-scale knowledge base which encodes relational
knowledge of entities into a graph structure.
In knowledge graphs, a fact is typically stored as a triple $(h,r,t)$, 
which indicates that entity $h$ and entity $t$ have relation $r$.
There are many famous knowledge graphs such as WordNet \cite{wordnet}, Freebase \cite{freebase}, YAGO \cite{yago}, and Wikidata \cite{wikidata}. 
They have been widely adopted in various applications,
such as recommendation systems \cite{rs}, question answering \cite{qa}, reading comprehension \cite{reading}, to name a few.

A fundamental challenge in researches and applications related to knowledge graphs is representation learning.
It aims to learn low-dimensional continuous embeddings for the stored entities and relations with semantic knowledge encoded.
Various methods have been proposed to achieve this goal,
such as TransE \cite{TransE}, TransH \cite{TransH}, and HolE \cite{HolE}.

It is noticeable that most existing methods completely ignore available temporal information in both learning and inference phases,
which means facts must be static or time-unaware \cite{tTransE}, i.e., triple $(h, r, t)$ cannot include temporal information.
However, many facts are valid only in a specific period \cite{HyTE}.
For example, the fact ``Andre Agassi is a professional tennis player''
is formalized as (Andre Agassi, is, professional tennis player) in a knowledge graph,
but it only holds from 1986 to 2006
as he started playing tennis as a professional player in 1986 and retired in 2006.

It is evident that temporal information can help learn more credible embeddings for 
both entities and relations.
The literature primarily proposed two kinds of approaches,
one of them preserves temporal order in the learned embeddings like t-TransE \cite{tTransE},
and the other encodes temporal information explicitly.
TA-TransE \cite{timeSequence} and HyTE \cite{HyTE} are two
algorithms in this direction. 
TA-TransE first converts temporal information into temporal tokens (i.e.,
words), then learns the semantics of these tokens using recurrent neural networks.
HyTE projects entities and relations onto the same time-dependent
hyperplane, then applies the TransE condition \cite{TransE} to the projected embedding vectors.
Like the translation-based representation learning methods for static knowledge graphs,
HyTE is simple yet effective,
getting state-of-the-art results.

In this paper,
we concentrates on encoding temporal information explicitly
by extending HyTE to achieve much better results,
while preserving the simplicity of it.
First, instead of projecting embeddings of entities and relations to time-specific hyperplanes as in HyTE,
we transform embeddings between spaces.
Second, the effect of temporal information on the embeddings of entities
and relations is still not well understood,
existing models are based on different assumptions
and no rigid analysis is provided.
To address this problem,
we make three assumptions on whether embeddings of entities or relations are time-sensitive,
and build three different models corresponding to them.
We compare their experimental results to choose the most effecitive one.

To be more specific,
we incorporate starting and ending times in the fact triple $(h, r, t)$ to encode the temporal information explicitly.
Thus, a fact is then denoted as $(h,r,t,\tau_s,\tau_e)$, 
which indicates entities $h$ and $t$ have relation $r$ from $\tau_s$ to $\tau_e$.
We make three assumptions:
only entity is time-sensitive,
only relation is time-sensitive,
and both entity and relation are time-sensitive.
Three models are built corresponding to these assumptions,
referred to as BTS, ETS, and RTS.
For all three models,
we build two separate embedding spaces for relations and entities,
different time-specific transformation matrices between these spaces are learned
according to the model assumptions adopted.

We evaluate all the models on three tasks:
entity prediction, relation prediction, and temporal scope prediction.
The proposed \rel\ model achieves the state-of-the-art results on all tasks,
showing the effectiveness of our models.
We further compare and analyze the results of three proposed models
and find that the influence of temporal information on relations are more important
than on entities for translation-based embedding learning methods.
This observation can also be adapted to simplify
other algorithms, such as HyTE.

Our contributions are listed as follows:
 \begin{itemize}
     \item We propose three new models to learn time-aware representations of
         knowledge graphs, among which the \rel{} model is simple and  effective, 
         getting state-of-the-art results on three evaluation tasks.
     \item We explore how temporal information affects the embeddings of
         entities and relations.
	     Experiments show that relation embeddings form nature ordering while 
         the influence of time on entity embeddings can be ignored for translation-based methods, 
         such as our models and HyTE.
     \item We show that existing models (e.g., HyTE) can be simplified based on our findings by only considering relations as time-sensitive and get equally good results.
 \end{itemize}
